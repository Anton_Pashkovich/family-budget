package by.rfe.budget.controller;

import by.rfe.budget.entity.Entry;
import by.rfe.budget.entity.Family;
import by.rfe.budget.entity.Person;
import by.rfe.budget.repository.EntryRepository;
import by.rfe.budget.repository.PersonRepository;
import by.rfe.budget.service.EntryService;
import by.rfe.budget.service.FamilyService;
import by.rfe.budget.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

@Controller
public class AppController {

    @Autowired
    private PersonService personService;

    @Autowired
    private EntryService entryService;

    @Autowired
    private FamilyService familyService;

    @RequestMapping(value = "/login")
    public String loginPage() {
        return "login";
    }

    @RequestMapping(value = "/")
    public String budget(Model model, Principal principal, SearchForm searchForm) {
        Integer familyId = getFamilyId(principal);
        model.addAttribute("familyId", familyId);
        model.addAttribute("balance", getBalance(familyId));
        List<Entry> incomeList = entryService.getIncomes(familyId, searchForm);
        model.addAttribute("incomes", incomeList);
        model.addAttribute("incomeValue", entryService.getSum(incomeList));
        List<Entry> outcomeList = entryService.getOutcomes(familyId, searchForm);
        model.addAttribute("outcomes", outcomeList);
        model.addAttribute("outcomeValue", entryService.getSum(outcomeList));
        model.addAttribute("searchForm", searchForm);
        return "main";
    }

    @RequestMapping(value = "/entry/delete/{id}")
    public void deleteEntry(@PathVariable(value = "id") Integer id, HttpServletRequest request, HttpServletResponse response) throws IOException {
        entryService.delete(id);
        String referer = request.getHeader("referer");
        response.sendRedirect(referer);
    }

    @RequestMapping(value = "/addEntry")
    public String addEntryPage(Model model, Principal principal) {
        Integer familyId = getFamilyId(principal);
        model.addAttribute("balance", getBalance(familyId));
        model.addAttribute("entry", new Entry());
        return "add-entry";
    }

    @RequestMapping(value = "/addEntry/{direction}", method = RequestMethod.POST)
    public String addEntry(@PathVariable(value = "direction") boolean direction, Entry entry, Principal principal) {
        Integer familyId = getFamilyId(principal);
        entry.setFamilyId(familyId);
        entry.setDirection(direction);
        entryService.create(entry);
        return "redirect:/";
    }

    @RequestMapping(value = "/family")
    public String familyPage(Model model, Principal principal) {
        Integer familyId = getFamilyId(principal);
        model.addAttribute("persons", personService.findAll(familyId));
        model.addAttribute("person", new Person());
        return "family";
    }

    @RequestMapping(value = "/person/create", method = RequestMethod.POST)
    public String addPerson(Person person, Principal principal, Model model, RedirectAttributes redirectAttributes) {
        Integer familyId = getFamilyId(principal);
        if (personService.getByLogin(person.getLogin()) != null) {
            redirectAttributes.addFlashAttribute("loginError", true);
            return "redirect:/family";
        }
        if (!person.getPassword().equals(person.getConfirmPassword())) {
            redirectAttributes.addFlashAttribute("passwordError", true);
            return "redirect:/family";
        }
        person.setFamilyId(familyId);
        personService.create(person);
        return "redirect:/family";
    }

    @RequestMapping(value = "person/delete/{id}")
    public String deletePerson(@PathVariable(value = "id") Integer id) {
        personService.delete(id);
        return "redirect:/family";
    }

    @RequestMapping(value = "/registration")
    public String registrationPage(Model model) {
        model.addAttribute("person", new Person());
        return "registration";
    }

    @RequestMapping(value = "/family/create", method = RequestMethod.POST)
    public String createFamily(Person person, Model model, RedirectAttributes redirectAttributes) {
        if (personService.getByLogin(person.getLogin()) != null) {
            redirectAttributes.addFlashAttribute("loginError", true);
            return "redirect:/registration";
        }
        if (!person.getPassword().equals(person.getConfirmPassword())) {
            redirectAttributes.addFlashAttribute("passwordError", true);
            return "redirect:/registration";
        }
        Family family = familyService.create(new Family());
        person.setFamilyId(family.getId());
        personService.create(person);
        redirectAttributes.addAttribute("sussess", true);
        return "redirect:/";
    }

    private Integer getBalance(Integer familyId) {
        List<Entry> incomes = entryService.getIncomes(familyId);
        List<Entry> outcomes = entryService.getOutcomes(familyId);
        Integer income = entryService.getSum(incomes);
        Integer outcome = entryService.getSum(outcomes);
        return income - outcome;
    }

    private Integer getFamilyId(Principal principal) {
        String name = principal.getName();
        Person person = personService.getByLogin(name);
        return person.getFamilyId();
    }

}
