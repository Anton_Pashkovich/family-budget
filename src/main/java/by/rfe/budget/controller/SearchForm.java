package by.rfe.budget.controller;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class SearchForm {

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date start;
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date end;

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

}
