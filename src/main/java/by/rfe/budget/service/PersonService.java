package by.rfe.budget.service;

import by.rfe.budget.entity.Person;
import by.rfe.budget.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("personService")
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public void create(Person person) {
        personRepository.saveAndFlush(person);
    }

    public Person read(Integer id) {
        return personRepository.getOne(id);
    }

    public void update(Person person) {
        personRepository.saveAndFlush(person);
    }

    public void delete(Integer id) {
        personRepository.delete(id);
    }

    public Person getByLogin(String login) {
        return personRepository.findByLogin(login);
    }

    public List<Person> findAll(Integer familyId) {
        return personRepository.findAll(familyId);
    }

}
