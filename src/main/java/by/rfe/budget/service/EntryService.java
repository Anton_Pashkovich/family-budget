package by.rfe.budget.service;

import by.rfe.budget.controller.SearchForm;
import by.rfe.budget.entity.Entry;
import by.rfe.budget.repository.EntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service("entryService")
public class EntryService {

    @Autowired
    private EntryRepository entryRepository;

    public void create(Entry entry) {
        entryRepository.saveAndFlush(entry);
    }

    public Entry read(Integer id) {
        return entryRepository.getOne(id);
    }

    public void update(Entry entry) {
        entryRepository.saveAndFlush(entry);
    }

    public void delete(Integer id) {
        entryRepository.delete(id);
    }

    public List<Entry> getIncomes(Integer familyId) {
        return entryRepository.getIncomes(familyId);
    }

    public List<Entry> getIncomes(Integer familyId, SearchForm searchForm) {
        Date start = searchForm.getStart();
        Date end = searchForm.getEnd();
        if (start == null && end == null) {
            return entryRepository.getIncomes(familyId);
        } else if (start != null && end == null) {
            return entryRepository.getIncomesByStartDate(familyId, start);
        } else if (start == null && end != null) {
            return entryRepository.getIncomesByEndDate(familyId, getEnd(end));
        } else {
            return entryRepository.getIncomes(familyId, start, getEnd(end));
        }
    }

    public List<Entry> getOutcomes(Integer familyId) {
        return entryRepository.getOutcomes(familyId);
    }

    public List<Entry> getOutcomes(Integer familyId, SearchForm searchForm) {
        Date start = searchForm.getStart();
        Date end = searchForm.getEnd();
        if (start == null && end == null) {
            return entryRepository.getOutcomes(familyId);
        } else if (start != null && end == null) {
            return entryRepository.getOutcomesByStartDate(familyId, start);
        } else if (start == null && end != null) {
            return entryRepository.getOutcomesByEndDate(familyId, getEnd(end));
        } else {
            return entryRepository.getOutcomes(familyId, start, getEnd(end));
        }
    }

    public List<Entry> getAll() {
        return entryRepository.findAll();
    }

    public Integer getSum(List<Entry> entries) {
        Integer sum = 0;
        for (Entry entry : entries) {
            sum += entry.getValue();
        }
        return sum;
    }

    private Date getEnd(Date end) {
        Calendar c = Calendar.getInstance();
        c.setTime(end);
        c.add(Calendar.DATE, 1);
        return c.getTime();
    }

}
