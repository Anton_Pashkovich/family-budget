package by.rfe.budget.service;

import by.rfe.budget.entity.Family;
import by.rfe.budget.repository.FamilyRepositiry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("familyService")
public class FamilyService {

    @Autowired
    private FamilyRepositiry familyRepositiry;

    public Family create(Family family) {
        return familyRepositiry.saveAndFlush(family);
    }

    public Family read(Integer id) {
        return familyRepositiry.getOne(id);
    }

    public void update (Family family) {
        familyRepositiry.saveAndFlush(family);
    }

    public void delete (Integer id) {
        familyRepositiry.delete(id);
    }

}