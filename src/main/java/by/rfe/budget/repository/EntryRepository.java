package by.rfe.budget.repository;

import by.rfe.budget.entity.Entry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface EntryRepository extends JpaRepository<Entry, Integer> {

    @Query("select e from Entry e where e.familyId = :familyId and e.direction = true order by date asc")
    public List<Entry> getIncomes(@Param("familyId") Integer familyId);

    @Query("select e from Entry e where e.familyId = :familyId and e.date >= :start and e.direction = true order by date asc")
    public List<Entry> getIncomesByStartDate(@Param("familyId") Integer familyId, @Param("start") Date start);

    @Query("select e from Entry e where e.familyId = :familyId and e.date <= :end and e.direction = true order by date asc")
    public List<Entry> getIncomesByEndDate(@Param("familyId") Integer familyId, @Param("end") Date end);

    @Query("select e from Entry e where e.familyId = :familyId and e.date >= :start and e.date <= :end and e.direction = true order by date asc")
    public List<Entry> getIncomes(@Param("familyId") Integer familyId, @Param("start") Date start, @Param("end") Date end);

    @Query("select e from Entry e where e.familyId = :familyId and e.direction = false order by date asc")
    public List<Entry> getOutcomes(@Param("familyId") Integer familyId);

    @Query("select e from Entry e where e.familyId = :familyId and e.date >= :start and e.direction = false order by date asc")
    public List<Entry> getOutcomesByStartDate(@Param("familyId") Integer familyId, @Param("start") Date start);

    @Query("select e from Entry e where e.familyId = :familyId and e.date <= :end and e.direction = false order by date asc")
    public List<Entry> getOutcomesByEndDate(@Param("familyId") Integer familyId, @Param("end") Date end);

    @Query("select e from Entry e where e.familyId = :familyId and e.date >= :start and e.date <= :end and e.direction = false order by date asc")
    public List<Entry> getOutcomes(@Param("familyId") Integer familyId, @Param("start") Date start, @Param("end") Date end);
}
