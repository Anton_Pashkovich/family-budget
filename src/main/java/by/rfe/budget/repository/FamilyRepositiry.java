package by.rfe.budget.repository;

import by.rfe.budget.entity.Family;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FamilyRepositiry extends JpaRepository<Family, Integer> {
}
