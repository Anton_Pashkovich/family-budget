package by.rfe.budget.repository;

import by.rfe.budget.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PersonRepository extends JpaRepository<Person, Integer> {

    Person findByLogin(@Param("login") String login);

    @Query("select p from Person p where p.familyId = :familyId order by login asc")
    List<Person> findAll(@Param("familyId") Integer familyId);

}
