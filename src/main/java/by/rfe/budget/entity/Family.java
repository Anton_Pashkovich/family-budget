package by.rfe.budget.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Family")
public class Family {

    @Id
    @SequenceGenerator(name="FAMILY_SEQUENCE", sequenceName="FAMILY_SEQUENCE", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FAMILY_SEQUENCE")
    @Column(name = "family_id")
    private Integer id;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "family_id")
    private List<Person> persons;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "family_id")
    private List<Entry> entryList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    public List<Entry> getEntryList() {
        return entryList;
    }

    public void setEntryList(List<Entry> entryList) {
        this.entryList = entryList;
    }

}
